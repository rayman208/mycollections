﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCollection
{
    class Program
    {
        class Man
        {
            public int Age { get; set; }
            public string Name { get; set; }

            public override string ToString()
            {
                return $"{Name}: {Age}";
            }
        }

        static void Main(string[] args)
        {
            ArrayCollection<Man> collection = new ArrayCollection<Man>();

            collection.Add(new Man()
                { Age = 15,Name = "Anton"}
            );
            collection.Add(new Man()
                { Age = 18, Name = "Gena" }
            );
            collection.Add(new Man()
                { Age = 25, Name = "Nikolay" }
            );

            //collection[0] = 999;

            collection.RemoveAt(1);

            /*for (int i = 0; i < collection.Length; i++)
            {
                Console.Write(collection[i]+" ");
            }
            Console.WriteLine();*/
            
            foreach (Man item in collection)
            {
                Console.WriteLine(item + " ");
            }

            Console.ReadKey();
        }
    }
}
