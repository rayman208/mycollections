﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCollection
{
    class ArrayCollection<T>:IEnumerable
    {
        private T[] array;
        private int length;
        private int capacity;

        public ArrayCollection()
        {
            array = new T[1];
            length = 0;
            capacity = 1;
        }

        public int Length
        {
            get { return length; }
        }

        public T this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }

        public void Add(T item)
        {
            length++;

            if (length > capacity)
            {
                capacity *= 2;

                T[] tempArray = new T[capacity];

                for (int i = 0; i < length - 1; i++)
                {
                    tempArray[i] = array[i];
                }

                array = tempArray;
            }

            array[length - 1] = item;
        }

        public void RemoveAt(int index)
        {
            for (int i = index; i < array.Length - 1; i++)
            {
                array[i] = array[i + 1];
            }

            length--;
        }

        public IEnumerator GetEnumerator()
        {
            return new ArrayCollectionEnumerator<T>(this);
        }
    }

    class ArrayCollectionEnumerator<T> : IEnumerator
    {
        private ArrayCollection<T> collection;
        int index = -1;

        public ArrayCollectionEnumerator(ArrayCollection<T> collection)
        {
            this.collection = collection;
        }

        public bool MoveNext()
        {
            index++;
            return index < collection.Length;
        }

        public void Reset()
        {
            index = -1;
        }

        public object Current
        {
            get
            {
                return collection[index];
            }
        }
    }
}
